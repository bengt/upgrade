Upgrade
=======

A shorthand for `yum upgrade`.

Downloading
-----------

    git clone git@github.com:bigben87/upgrade.git

Installing
----------

    sudo ln -s `pwd`/upgrade/upgrade.sh /usr/bin/upgrade

Running
-------

    sudo upgrade